# /etc/cron.d/hcoop-apache-sync-logs: Copy apache logs to user
# homedirs every 20 minutes.
MAILTO=logs@hcoop.net

0 */2 * * * root /afs/hcoop.net/common/etc/scripts/apache-sync-logs >/dev/null 2>&1
